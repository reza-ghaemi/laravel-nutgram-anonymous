<?php
/** @var SergiX44\Nutgram\Nutgram $bot */

use SergiX44\Nutgram\Nutgram;
use App\Models\TelegramUser;
use App\Models\Message;


/*
|--------------------------------------------------------------------------
| Nutgram Handlers
|--------------------------------------------------------------------------
|
| Here is where you can register telegram handlers for Nutgram. These
| handlers are loaded by the NutgramServiceProvider. Enjoy!
|
*/
$bot->onCommand("start" ,function (Nutgram $bot){
    $bot->sendMessage("hello , i am your anonymous chat assistant. lets start.");
})->middleware(function (Nutgram $bot, $next){
    $telegram_user = $bot->user();
    $username = '@'.$telegram_user->username;
    $name = $telegram_user->first_name . " " . $telegram_user->last_name;


//   $user = TelegramUser::where(['user_id' => $username , 'seen' => false])->first();
//   if ($user){
       $messages = Message::where('receiver_id' , $username)->where('seen' , false)->get();
       $num = $messages->count();
       $bot->sendMessage("you have $num unseen messages ");
       if ( $num != 0 ) {
       foreach($messages as $message)
           {
               $bot->sendMessage($message->content . '\n');
               $message->seen = true;
               $message->save();

           }
       }
//   } else {
//       $user = TelegramUser::create(['name' => $name ,'user_id' => $username]);
//   }

   $next($bot);
});


;
$bot->onCommand("help" ,function (Nutgram $bot){
    $bot->sendMessage("this bot is intended to provide AnonymousChat,
            is there someone you may want to send message to anonymously?
            les start .");
});
$bot->onCommand("contact us" ,function (Nutgram $bot){
    $bot->sendMessage("ghaemi.reza.94@gmail.com");
});

$bot->onCommand('chat',\App\Http\Conversations\AnonyMousChatConversation::class);
