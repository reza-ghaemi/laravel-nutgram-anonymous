<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('telegram_users', function (Blueprint $table) {
            $table->string('user_id');
            $table->index('user_id');
            $table->string('name');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');


        });
    }

};
