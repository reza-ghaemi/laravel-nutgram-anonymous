<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramUser extends User
{
    use HasFactory;

    protected $keyType = 'string';
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'name',
        'user_id'
    ];

    protected function casts(): array
    {
        return [
            'name' => 'string',
            'user_id' => 'string',
        ];
    }

    public $incrementing = false;
}
