<?php

namespace App\Http\Conversations;

use App\Http\Controllers\SendTextToUsers;
use SergiX44\Nutgram\Nutgram;
use SergiX44\Nutgram\Telegram\Properties\UpdateType;
use SergiX44\Nutgram\Telegram\Types\Keyboard\InlineKeyboardButton;
use SergiX44\Nutgram\Telegram\Types\Keyboard\InlineKeyboardMarkup;



class AnonyMousChatConversation extends \SergiX44\Nutgram\Conversations\Conversation
{

    protected ?string $step = 'start';

    protected $target_user_id;

    public function cacheTargetUserId(string $user_id)
    {
        $this->target_user_id = $user_id;
    }

    public function start(Nutgram $bot)
    {
        $bot->sendMessage(
            text: 'user id?'
        );
        $this->next('second');
    }
    public function second(Nutgram $bot)
    {
        $this->target_user_id = $bot->message()->text;
        $bot->sendMessage('is this your desired id ? ' . ' ' . $this->target_user_id);
        $this->next('third');
    }
    public function third(Nutgram $bot)
    {

        $answer = $bot->message()->text;
        if (strtolower($answer) === "yes") {
            $bot->sendMessage('please enter your desired text:');
            $this->next("fourth");
        }
        else {
            $this->start($bot);
        }


    }
    public function fourth(Nutgram $bot)
    {

        $text = (string)$bot->message()->text;
        $sender_id = (string)$bot->user()->username;

        SendTextToUsers::sendTextToUser($sender_id,$this->target_user_id,$text);

        $bot->sendMessage('message sent to :' . ' ' . $this->target_user_id);
        $this->end();
    }



}
