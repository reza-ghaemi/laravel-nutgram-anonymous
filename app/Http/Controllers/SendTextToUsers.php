<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use Nutgram\Laravel\Facades\Telegram;
use function Laravel\Prompts\text;

class SendTextToUsers extends Controller
{


    public static function sendTextToUser(string $sender_id ,string $receiver_id, string $text)
    {
        Message::create(['sender_id' => $sender_id,'receiver_id' => $receiver_id , 'content' => $text , 'seen' => false]);

    }

}
